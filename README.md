# Docker Workshop
Lab 04: Building your first image

---

## Overview

In this lab you will create your first image (a basic python web server). In addition, we will see how the CMD instruction can be overwritten from the docker run command.

## Preparations

 - Create a new folder for the lab:
```
$ mkdir ~/lab-04
$ cd ~/lab-04
```

## Instructions

 - Create a new file called "Dockerfile" with the following content:
```
nano ~/lab-04/Dockerfile
```

 - Set the content below
```
FROM selaworkshops/alpine:3.4
RUN apk-install python
CMD python -m SimpleHTTPServer 5000
```

 - Build the image using the command:
```
$ docker build -t python-app:1.0 .
```

 - Ensure the image was created:
```
$ docker images
```

 - Run the created image in interactive mode to attach to the container process:
```
$ docker run -it python-app:1.0
```

 - Browse to the port 5000 to check if you can reach the application:
```
http://<your-server-ip>:5000
```

 - The port 5000 is not exposed, therefore we can't reach the application. We will learn how to expose container ports in the **next module**.

 - Exit from the container using:
```
$ CTRL + C
```

 - Now, run the same image but passing a command as run parameter:
```
$ docker run python-app:1.0 "echo" "override command"
```

 - As you may have noticed the command inside the container was overridden by the command passed as parameter in the docker run command
 
 